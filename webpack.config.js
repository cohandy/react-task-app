module.exports = {
	entry: './src/index.jsx',
	output: {
		path: __dirname,
		filename: './public/bundle.js'
	},
	module: {
		loaders: [
			{ test: /\.(js|jsx)$/, loader: 'babel-loader', query: { presets: ['es2015', 'react'] } },
			{ test: /\.scss$/, loader: 'style-loader!css-loader!sass-loader' }
		]
	}
}