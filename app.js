var express = require('express'),
	config = require('./server/config'),
	app = express(),
	mongoose = require('mongoose');

app.set('port', process.env.PORT || 3300);

app = config(app);

//set up db
/*
mongoose.connect('mongodb://localhost:27017/todo-app');
mongoose.connection.on('open', function() {
	console.log('mongoose connected');
});
*/
mongoose.connect('mongodb://task-app:ipodnano777@ds149567.mlab.com:49567/heroku_f5q7sb2q');
mongoose.connection.on('open', function() {
	console.log('mongoose connected');
});


app.listen(app.get('port'), function() {
	console.log("Server up at " + app.get('port'));
});