var path = require('path'),
	routes = require('./routes'),
	express = require('express'),
	bodyParser = require('body-parser'),
	cookieParser = require('cookie-parser'),
	morgan = require('morgan'),
	session = require('express-session'),
	methodOverride = require('method-override');

module.exports = function(app) {
	app.use(morgan('dev'));
	app.use(methodOverride());
	app.use(cookieParser('test'));
	app.use(session({
		name: 'session-cookie-id',
		secret: 'test',
		saveUninitialized: true,
		resave: false,
		cookie: { httpOnly: false, secret: 'test' }
	}));
	app.use(bodyParser.urlencoded({ extended: false}));
	app.use(bodyParser.json());
	app.use('/public/', express.static(path.join(__dirname, '../public')));
	routes(app);

	//error handler
	app.use(function(err, req, res) {
		console.log(err);
		if(req.xhr) {
			res.json({'success': 'false', 'error': 'No database connection'});
		}
	});

	return app;
}