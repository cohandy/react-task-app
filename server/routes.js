var express = require('express'),
	router = express.Router(),
	controllers = require('../controllers/index');

module.exports = function(app) {
	//home routes
	router.get('/', controllers.home.index);
	router.get('/projects', controllers.home.index);
	router.get('/project/:name', controllers.home.index);
	router.post('/login', controllers.home.login);
	router.post('/logout', controllers.home.logout);
	//project routes
	router.post('/projects/fetch', controllers.project.fetch);
	router.post('/projects/create', controllers.project.create);
	router.post('/projects/delete', controllers.project.delete);
	//task routes
	router.post('/tasks/create', controllers.task.create);
	router.post('/tasks/fetch', controllers.task.fetch);
	router.post('/tasks/complete', controllers.task.complete);
	router.post('/tasks/delete', controllers.task.delete);
	app.use('/', router);
}