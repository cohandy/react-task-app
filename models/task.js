var mongoose = require('mongoose'),
	Schema = mongoose.Schema;

var TaskSchema = new Schema({
	task: {
		type: String,
		minlength: [1, "Task must be at least one character long"]
	},
	completed: { type: Boolean, default: false },
	projectId: { type: Schema.Types.ObjectId }
});

module.exports = mongoose.model('task', TaskSchema);