var mongoose = require('mongoose'),
	Schema = mongoose.Schema;

var ProjectSchema = new Schema({
	title: {
		type: String,
		minlength: [3, "Title must be at least 3 characters"],
		maxlength: [25, "Title cannot exceed 25 characters"]
	},
	userId: {
		type: Schema.Types.ObjectId
	}
});

module.exports = mongoose.model('project', ProjectSchema);