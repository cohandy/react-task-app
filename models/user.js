var mongoose = require('mongoose'),
	uniqueValidator = require('mongoose-unique-validator'),
	Schema = mongoose.Schema;

var UserSchema = new Schema({
	username: {
		type: String,
		required: [true, 'Username is required'],
		unique: true,
		uniqueCaseInsensitive: true,
		minlength: [3, "Username must be at least 3 characters"],
		maxlength: [25, "Username cannot exceed 25 characters"]
	}
});

UserSchema.plugin(uniqueValidator, { message: '{VALUE} has already been taken!'});

module.exports = mongoose.model('user', UserSchema);