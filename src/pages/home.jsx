var React = require('react'),
	path = require('path');

import { Redirect } from 'react-router-dom';
import UserStore from '../stores/user-store';
import * as UserActions from "../actions/user-actions"

export default class Home extends React.Component {
	constructor(props) {
		super(props);
		//bind event methods
		this.setUser = this.setUser.bind(this);
		this.setError = this.setError.bind(this);
		this.isLoading = this.isLoading.bind(this);
		//set state
		this.state = {
			user: UserStore.getUser(),
			error: null,
			loading: false
		}
	}

	//lifecycle methods

	componentWillMount() {
		UserStore.on("change", this.setUser);
		UserStore.on("error", this.setError);
		UserStore.on("loading", this.isLoading);
	}

	componentWillUnmount() {
		UserStore.removeListener("change", this.setUser);
		UserStore.removeListener("error", this.setError);
		UserStore.removeListener("loading", this.isLoading);
	}

	componentDidMount() {
		this._input.focus();
	}

	//event functions

	setUser() {
		this.setState({
			user: UserStore.getUser(),
			error: null,
			loading: false
		});
	}

	setError() {
		this.setState({
			error: UserStore.getError(),
			loading: false
		});
	}

	isLoading() {
		this.setState({
			loading: true
		});
	}

	//render state checks

	renderCheck() {
		return {
			error: () => {
				var errorStyle = {}
				if(this.state.error) {
					errorStyle = {
						bottom: '-35px'
					}
					this._inputError.innerHTML = this.state.error;
				}
				return errorStyle;
			},
			loading: () => {
				var loading = "";
				if(this.state.loading) {
					loading = (
						<div id="loading-div">
							<img src={path.join(__dirname, '../../public/images/puff.svg')}/>
						</div>
					);
				}
				return loading;
			}
		}
	}

	submitForm(e) {
		if(e) e.preventDefault();
		UserActions.login(this._input.value)
	}

	//render

	render() {
		//check login session
		if(this.state.user.loggedIn) {
			return (
				<Redirect to="/projects"/>
			)
		}
		var renderCheck = this.renderCheck();
		//check errors
		var errorStyle = renderCheck.error();
		//if loading ajax
		var loading = renderCheck.loading();
		return (
			<div className="container" id="login-container">
				<h1>Welcome to Task App</h1>
				<div className="input-div">
					<form onSubmit={this.submitForm.bind(this)}>
						<input className="theme-input" type="text" ref={(el) => this._input = el } placeholder="Enter your name"/>
						<i onClick={this.submitForm.bind(this)} className="fa fa-arrow-right"></i>
						<div className="home-input-error" ref={(el) => this._inputError = el } style={errorStyle}></div>
					</form>
				</div>
				{loading}
			</div>
		)
	}
}