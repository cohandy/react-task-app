var React = require('react'),
	ReactDOM = require('react-dom'),
	path = require('path');

import { Redirect, Link } from 'react-router-dom';
import UserStore from '../stores/user-store';
import ProjectStore from '../stores/project-store';
import * as ProjectActions from '../actions/project-actions';
import * as TaskActions from '../actions/task-actions';

export default class ProjectList extends React.Component {
	constructor(props) {
		super(props);
		this.setUser = this.setUser.bind(this);
		this.projectChange = this.projectChange.bind(this);
		this.state = {
			user: UserStore.getUser(),
			project: ProjectStore.getProjects(),
			renderChecks: ProjectStore.getRenderChecks(),
			inputActive: false
		}
	}

	//lifecycle methods

	componentWillMount() {
		UserStore.on("change", this.setUser);
		ProjectStore.on("change", this.projectChange);
	}

	componentWillUnmount() {
		UserStore.removeListener("change", this.setUser);
		ProjectStore.removeListener("change", this.projectChange);
	}

	componentDidMount() {
		ProjectActions.fetch();
	}

	//event functions

	projectChange() {
		this.setState({
			project: ProjectStore.getProjects(),
			renderChecks: ProjectStore.getRenderChecks()
		});
	}

	setUser() {
		this.setState({
			user: UserStore.getUser()
		});
	}

	//page behavior

	slideInput() {
		this.setState({
			inputActive: !this.state.inputActive
		});
		this._input.focus();
	}

	submitProject(e) {
		if(e) e.preventDefault();
		ProjectActions.create(this._input.value);
		this._input.value = "";
		this.setState({
			inputActive:false
		});
	}

	//project div behavior

	deleteProject(e) {
		e.preventDefault();
		var projectId = e.target.parentNode.parentNode.getAttribute('data-id');
		ProjectActions.destroy(projectId);
	}

	//render state checks

	renderChecks() {
		return {
			createError: () => {
				var errorStyle = {}
				if(this.state.renderChecks.errors.create) {
					errorStyle = {
						height: '30px'
					}
					this._createInput.innerHTML = this.state.renderChecks.errors.create;
				} else {
					errorStyle = {
						height: '0'
					}
				}
				return errorStyle;
			},
			fetchError: () => {
				var fetchError = "";
				if(this.state.renderChecks.errors.fetch) {
					fetchError = <div>{this.state.renderChecks.errors.fetch}</div>
				}
				return fetchError;
			},
			loading: () => {
				var loading = "";
				if(this.state.renderChecks.loading) {
					loading = (
						<div id="loading-div">
							<img src={path.join(__dirname, '../../public/images/puff.svg')}/>
						</div>
					);
				}
				return loading;
			},
			addInput: () => {
				var inputStyle = {}
				if(this.state.inputActive) {
					inputStyle = {
						height: '55px'
					}
				} else {
					inputStyle = {
						height: '0px'
					}
				}
				return inputStyle;
			},
			empty: () => {
				var empty = "";
				if(!this.state.project.projects.length && !this.state.renderChecks.loading) {
					empty = <div className="empty-list">Add a project by clicking on the plus icon above</div>
				}
				return empty;
			}
		}
	}

	//render

	render() {
		if(!this.state.user.loggedIn) {
			return (
				<Redirect to="/"/>
			)
		}
		//render state changes
		var checks = this.renderChecks(),
			errorStyle = checks.createError(),
			fetchError = checks.fetchError(),
			loading = checks.loading(),
			empty = checks.empty(),
			inputStyle = checks.addInput();

		//project links
		const projects = this.state.project.projects;

		const projectList = projects.map((project, index) => {
			return <Project {...project} key={"project_" + index} callback={ this.deleteProject.bind(this) }/>
		});

		return (
			<div className="container" id="project-container">
				<div className="page-header">
					<h3><strong>Projects</strong></h3>
					<i className="fa fa-plus-circle" onClick={() => this.slideInput()}></i>
					<form onSubmit={this.submitProject.bind(this)}>
						<div className="input-div" ref={(el) => this._inputDiv = el } style={inputStyle}>
							<input className="theme-input" type="text" placeholder="Project Title" ref={(el) => this._input = el }/>
							<div onClick={() => this.submitProject() } className="add-submit">
								<span>ADD</span>
							</div>
						</div>
						<div className="input-error" ref={(el) => this._createInput = el } style={errorStyle}></div>
					</form>
				</div>
				{ fetchError }
				{ empty }
				<div>
					{ projectList }
				</div>
				{ loading }
			</div>
		)
	}
}

const Project = (props) => {
	const title = props.title.split(' ').join('-').toLowerCase();
	return (
		<Link to={`project/${title}`} className="project-div" data-id={props._id} onClick={() => TaskActions.changeProject(props)}>
			<span className="title">{ props.title }</span>
			<div className="project-options">
				<i className="fa fa-remove remove" onClick={ props.callback }></i>
			</div>
		</Link>
	)
}
