var React = require('react'),
	ReactDOM = require('react-dom'),
	path = require('path');

import { Redirect, Link } from 'react-router-dom';

import UserStore from '../stores/user-store';
import TaskStore from "../stores/task-store";
import * as TaskActions from "../actions/task-actions";

export class TaskList extends React.Component {
	constructor(props) {
		super(props);
		this.setUser = this.setUser.bind(this);
		this.setTasks = this.setTasks.bind(this);
		this.state = {
			user: UserStore.getUser(),
			project: TaskStore.getCurrentProject(),
			tasks: TaskStore.getTasks(),
			renderChecks: TaskStore.getRenderChecks(),
			inputActive: false
		}
	}

	//lifecycle methods

	componentWillMount() {
		UserStore.on("change", this.setUser);
		TaskStore.on("change", this.setTasks);
	}

	componentWillUnmount() {
		UserStore.removeListener("change", this.setUser);
		TaskStore.removeListener("change", this.setTasks);
	}

	componentDidMount() {
		TaskActions.fetch();
	}

	//event methods

	setUser() {
		this.setState({
			user: UserStore.getUser()
		});
	}

	setTasks() {
		this.setState({
			tasks: TaskStore.getTasks(),
			renderChecks: TaskStore.getRenderChecks()
		});
	}

	//behavior methods

	slideInput() {
		this.setState({
			inputActive: !this.state.inputActive
		});
		this._input.focus();
	}

	submitTask(e) {
		if(e) e.preventDefault();
		TaskActions.create(this._input.value);
		this._input.value = "";
		this.setState({
			inputActive:false
		});
	}

	//render checks

	renderChecks() {
		return {
			createError: () => {
				var errorStyle = {}
				if(this.state.renderChecks.errors.create) {
					errorStyle = {
						height: '30px'
					}
					this._createInput.innerHTML = this.state.renderChecks.errors.create;
				} else {
					errorStyle = {
						height: '0'
					}
				}
				return errorStyle;
			},
			fetchError: () => {
				var fetchError = "";
				if(this.state.renderChecks.errors.fetch) {
					fetchError = <div>{this.state.renderChecks.errors.fetch}</div>
				}
				return fetchError;
			},
			loading: () => {
				var loading = "";
				if(this.state.renderChecks.loading) {
					loading = (
						<div id="loading-div">
							<img src={path.join(__dirname, '../../public/images/puff.svg')}/>
						</div>
					);
				}
				return loading;
			},
			addInput: () => {
				var inputStyle = {}
				if(this.state.inputActive) {
					inputStyle = {
						height: '55px'
					}
				} else {
					inputStyle = {
						height: '0px'
					}
				}
				return inputStyle;
			},
			empty: () => {
				var empty = "";
				if(!this.state.tasks.length && !this.state.renderChecks.loading) {
					empty = <div className="empty-list">Add a task by clicking on the plus icon above</div>
				}
				return empty;
			}
		}
	}


	//render

	render() {
		if(!this.state.user.loggedIn) {
			return (
				<Redirect to="/"/>
			)
		}

		//render state changes
		var checks = this.renderChecks(),
			errorStyle = checks.createError(),
			fetchError = checks.fetchError(),
			inputStyle = checks.addInput(),
			empty = checks.empty(),
			loading = checks.loading();

		const tasks = this.state.tasks;
		tasks.sort(function(a, b) {
			if(a.completed && b.completed) {
				return 0;
			} else if(b.completed) {
				return -1;
			} else {
				return 1;
			}
		})

		const taskComponents = tasks.map((task, index) => {
			return <Task {...task} key={`task_${index}`} index={index} />
		});

		return (
			<div className="container" id="task-container">
				<div className="page-header">
					<h3>
						<Link to="/projects" className="fa fa-angle-double-left link-arrow"></Link>
						Project - <strong>{this.state.project.title}</strong>
					</h3>
					<i className="fa fa-plus-circle" onClick={() => this.slideInput()}></i>
					<form onSubmit={this.submitTask.bind(this)}>
						<div className="input-div" ref={(el) => this._inputDiv = el } style={inputStyle}>
							<input className="theme-input" type="text" placeholder="Task" ref={(el) => this._input = el }/>
							<div onClick={() => this.submitTask() } className="add-submit">
								<span>ADD</span>
							</div>
						</div>
						<div className="input-error" ref={(el) => this._createInput = el } style={errorStyle}></div>
					</form>
				</div>
				{fetchError}
				<div>
					{taskComponents}
					{empty}
				</div>
				{loading}
			</div>
		)
	}
}

export class Task extends React.Component {
	constructor(props) {
		super(props);
	}

	complete(e) {
		var taskId = e.target.parentNode.parentNode.getAttribute('data-id');
		TaskActions.complete(taskId);
	}

	delete(e) {
		var taskId = e.target.parentNode.parentNode.getAttribute('data-id');
		TaskActions.destroy(taskId);
	}

	render() {
		var complete = "";
		if(this.props.completed) {
			complete = "task-div completed";
		} else complete = "task-div";
		return (
			<div className={complete} data-id={this.props._id}>
				<span className="task"><span>{this.props.index + 1}.</span>&nbsp;&nbsp;{ this.props.task }</span>
				<div className="project-options">
					{ !this.props.completed ? 
						<i className="fa fa-check-circle complete-icon" onClick={this.complete.bind(this)}></i> : <i></i>
					}
					<i className="fa fa-remove remove" onClick={this.delete.bind(this)}></i>
				</div>
			</div>
		)
	}
}