import { EventEmitter } from "events";
import dispatcher from "../dispatcher";

class TaskStore extends EventEmitter {
	constructor() {
		super();
		this.store = {
			currentProject: null,
			tasks: []
		}
		this.renderChecks = {
			loading: false,
			errors: {
				fetch: null,
				create: null
			}
		}
	}

	//getters

	getCurrentProject() {
		return this.store.currentProject;
	}

	getTasks() {
		return this.store.tasks;
	}

	getRenderChecks() {
		return this.renderChecks;
	}

	//actions

	change(project) {
		this.store.currentProject = project;
	}

	fetch() {
		this.store.tasks = [];
		this.renderChecks.loading = true;
		this.emit("change");
		fetch('/tasks/fetch', {
			method: "POST",
			body: JSON.stringify({
				projectId: this.store.currentProject._id
			}),
			headers: {
				'Content-Type': 'application/json'
			},
			credentials: 'same-origin'
		})
		.then((resp) => {
			if(resp.status === 200) {
				return resp.json();
			} else {
				throw resp.status;
			}
		})
		.then((json) => {
			if(json.success === "true") {
				this.store.tasks = json.tasks
				this.renderChecks.errors.fetch = null;
			} else {
				this.renderChecks.errors.fetch = "ERROR FETCHING TASKS";
			}
			this.renderChecks.loading = false;
			this.emit("change");
		})
		.catch((error) => {
			this.renderChecks.loading = false;
			this.renderChecks.errors.fetch = "DATABASE ERROR";
			this.emit("change");
		});
	}

	create(task) {
		this.renderChecks.loading = true;
		this.emit("change");
		fetch('/tasks/create', {
			method: "POST",
			body: JSON.stringify({
				task,
				projectId: this.store.currentProject._id
			}),
			headers: {
				'Content-Type': 'application/json'
			},
			credentials: 'same-origin'
		})
		.then((resp) => {
			if(resp.status === 200) {
				return resp.json();
			} else {
				throw resp.status;
			}
		})
		.then((json) => {
			if(json.success === "true") {
				this.store.tasks.push(json.task);
				this.renderChecks.errors.create = null;
			} else {
				this.renderChecks.errors.create = json.error;
			}
			this.renderChecks.loading = false;
			this.emit("change");
		})
		.catch((error) => {
			this.renderChecks.loading = false;
			this.renderChecks.errors.create = "DATABASE ERROR";
			this.emit("change");
		});
	}

	complete(taskId) {
		this.renderChecks.loading = true;
		this.emit("change");
		fetch('/tasks/complete', {
			method: "POST",
			body: JSON.stringify({
				taskId,
				projectId: this.store.currentProject._id
			}),
			headers: {
				'Content-Type': 'application/json'
			},
			credentials: 'same-origin'
		})
		.then((resp) => {
			if(resp.status === 200) {
				return resp.json();
			} else {
				throw resp.status;
			}
		})
		.then((json) => {
			if(json.success === "true") {
				var taskArr = this.store.tasks;
				for(var i=0;i<taskArr.length;i++) {
					if(taskArr[i]._id === json.task._id) {
						taskArr[i].completed = true;
						break;
					}
				}
				this.store.tasks = taskArr;
				this.renderChecks.errors.create = null;
			} else {
				this.renderChecks.errors.create = json.error;
			}
			this.renderChecks.loading = false;
			this.emit("change");
		})
		.catch((error) => {
			this.renderChecks.loading = false;
			this.renderChecks.errors.create = "DATABASE ERROR";
			this.emit("change");
		});
	}

	delete(taskId) {
		this.renderChecks.loading = true;
		this.emit("change");
		fetch('/tasks/delete', {
			method: "POST",
			body: JSON.stringify({
				taskId,
				projectId: this.store.currentProject._id
			}),
			headers: {
				'Content-Type': 'application/json'
			},
			credentials: 'same-origin'
		})
		.then((resp) => {
			if(resp.status === 200) {
				return resp.json();
			} else {
				throw resp.status;
			}
		})
		.then((json) => {
			if(json.success === "true") {
				var taskArr = this.store.tasks;
				for(var i=0;i<taskArr.length;i++) {
					if(taskArr[i]._id === json._id) {
						taskArr.splice(i, 1);
						break;
					}
				}
				this.store.tasks = taskArr;
				this.renderChecks.errors.create = null;
			} else {
				this.renderChecks.errors.create = json.error;
			}
			this.renderChecks.loading = false;
			this.emit("change");
		})
		.catch((error) => {
			this.renderChecks.loading = false;
			this.renderChecks.errors.create = "DATABASE ERROR";
			this.emit("change");
		});
	}

	handleActions(action) {
		switch(action.type) {
			case "CHANGE_PROJECT": {
				this.change(action.project);
				break;
			}
			case "CREATE_TASK": {
				this.create(action.task);
				break;
			}
			case "FETCH_TASKS": {
				this.fetch();
				break;
			}
			case "COMPLETE_TASK": {
				this.complete(action.taskId);
				break;
			}
			case "DELETE_TASK": {
				this.delete(action.taskId);
				break;
			}
		}
	}
}

const taskStore = new TaskStore;
dispatcher.register(taskStore.handleActions.bind(taskStore));

export default taskStore;