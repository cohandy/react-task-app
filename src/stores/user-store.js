import { EventEmitter } from "events";
import dispatcher from "../dispatcher";

class UserStore extends EventEmitter {
	constructor() {
		super();
		this.user = {
			loggedIn: false,
			username: null
		}
		this.error = null;
		this.loading = false;
	}

	getUser() {
		return this.user;
	}

	getError() {
		return this.error;
	}

	login(name) {
		this.emit('loading');
		fetch('/login', {
			method: "POST",
			body: JSON.stringify({username: name}),
			headers: {
				'Content-Type': 'application/json'
			},
			credentials: 'same-origin'
		})
		.then((resp) => {
			if(resp.status === 200) {
				return resp.json();
			} else {
				throw resp.status;
			}
		})
		.then((json) => {
			if(json.success === 'true') {
				this.user = {
					loggedIn: true,
					username: json.user.username
				}
				this.errors = [];
				this.emit("change");
			} else {
				this.error = json.errors.username.message;
				this.emit("error");
			}
		})
		.catch((err) => {
			this.error = "DATABASE ERROR";
			this.emit('error');
		});
	}

	logout() {
		this.user.loggedIn = false;
		this.emit("change");
		fetch('/logout', {
			method: "POST",
			body: "",
			headers: {
				'Content-Type': 'application/json'
			},
			credentials: 'same-origin'
		});
	}

	handleActions(action) {
		switch(action.type) {
			case "LOGIN_USER": {
				this.login(action.name);
				break;
			}
			case "LOGOUT_USER": {
				this.logout();
				break;
			}
		}
	}
}

const userStore = new UserStore;
dispatcher.register(userStore.handleActions.bind(userStore));

export default userStore;