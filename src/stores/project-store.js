import { EventEmitter } from 'events';
import dispatcher from '../dispatcher';

class ProjectStore extends EventEmitter {
	constructor() {
		super();
		this.project = {
			projects: []
		}
		this.renderChecks = {
			errors: {
				create: null,
				fetch: null
			},
			loading: false
		}
	}

	getProjects() {
		return this.project;
	}

	getRenderChecks() {
		return this.renderChecks;
	}

	fetch() {
		this.project.projects = [];
		this.renderChecks.loading = true;
		this.emit("change");
		fetch('/projects/fetch', {
			method: "POST",
			headers: {
				'Content-Type': 'application/json'
			},
			credentials: 'same-origin'
		})
		.then((resp) => {
			if(resp.status === 200) {
				return resp.json();
			} else {
				throw resp.status;
			}
		})
		.then((json) => {
			if(json.success === "true") {
				this.project.projects = json.projects;
				this.renderChecks.errors.fetch = null;
			} else {
				this.renderChecks.errors.fetch = "ERROR FETCHING PROJECTS";
			}
			this.renderChecks.loading = false;
			this.emit("change");
		})
		.catch((error) => {
			this.renderChecks.loading = false;
			this.renderChecks.errors.fetch = "DATABASE ERROR";
			this.emit("change");
		});
	}

	create(title) {
		this.renderChecks.loading = true;
		this.emit("change");
		fetch('/projects/create', {
			method: "POST",
			body: JSON.stringify({title: title}),
			headers: {
				'Content-Type': 'application/json'
			},
			credentials: 'same-origin'
		})
		.then((resp) => {
			if(resp.status === 200) {
				return resp.json();
			} else {
				throw resp.status;
			}
		})
		.then((resp) => {
			if(resp.success === "true") {
				this.project.projects.push(resp.project);
				this.renderChecks.errors.create = null;
			} else {
				this.renderChecks.errors.create = resp.errors.title.message;
			}
			this.renderChecks.loading = false;
			this.emit("change");
		})
		.catch((err) => {
			this.renderChecks.loading = false;
			this.renderChecks.errors.create = "DATABASE ERROR";
			this.emit("change");
		});
	}

	delete(id) {
		this.renderChecks.loading = true;
		this.emit("change");
		fetch('/projects/delete', {
			method: "POST",
			body: JSON.stringify({projectId: id}),
			headers: {
				'Content-Type': 'application/json'
			},
			credentials: 'same-origin'
		})
		.then((resp) => {
			if(resp.status === 200) {
				return resp.json();
			} else {
				throw resp.status;
			}
		})
		.then((resp) => {
			if(resp.success === "true") {
				var projArr = this.project.projects;
				for(var i=0;i<projArr.length;i++) {
					if(projArr[i]._id === resp._id) {
						projArr.splice(i, 1);
						break;
					}
				}
				this.project.projects = projArr;
				this.renderChecks.errors.create = null;
			} else {
				this.renderChecks.errors.create = resp.errors.title.message;
			}
			this.renderChecks.loading = false;
			this.emit("change");
		})
		.catch((err) => {
			this.renderChecks.loading = false;
			this.renderChecks.errors.create = "DATABASE ERROR";
			this.emit("change");
		});
	}

	handleActions(action) {
		switch(action.type) {
			case "FETCH": {
				this.fetch();
				break;
			}
			case "CREATE": {
				this.create(action.title);
				break;
			}
			case "DELETE": {
				this.delete(action.id);
				break;
			}
		}
	}
}

const projectStore = new ProjectStore;
dispatcher.register(projectStore.handleActions.bind(projectStore));

export default projectStore;