import dispatcher from "../dispatcher";

export function create(task) {
	dispatcher.dispatch({
		type: "CREATE_TASK",
		task,
	});
}

export function changeProject(project) {
	dispatcher.dispatch({
		type: "CHANGE_PROJECT",
		project,
	});
}

export function fetch() {
	dispatcher.dispatch({
		type: "FETCH_TASKS"
	});
}

export function complete(taskId) {
	dispatcher.dispatch({
		type: "COMPLETE_TASK",
		taskId
	});
}

export function destroy(taskId) {
	dispatcher.dispatch({
		type: "DELETE_TASK",
		taskId
	});
}