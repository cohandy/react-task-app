import dispatcher from "../dispatcher";

export function login(name) {
	dispatcher.dispatch({
		type: "LOGIN_USER",
		name,
	});
}

export function logout() {
	dispatcher.dispatch({
		type: "LOGOUT_USER"
	});
}