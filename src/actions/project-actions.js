import dispatcher from "../dispatcher";

export function fetch() {
	dispatcher.dispatch({
		type: 'FETCH'
	});
}

export function create(title) {
	dispatcher.dispatch({
		type: "CREATE",
		title,
	});
}

export function destroy(id) {
	dispatcher.dispatch({
		type: "DELETE",
		id
	});
}