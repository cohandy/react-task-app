var React = require('react'),
	ReactDOM = require('react-dom');

//require sass
require('./styles/index.scss');

import 'whatwg-fetch';
import { BrowserRouter as Router, Route, Link, Redirect, withRouter } from 'react-router-dom';

//import components
import Home from './pages/home.jsx';
import ProjectList from './pages/project.jsx'
import { TaskList, Task } from './pages/task.jsx';

import UserStore from './stores/user-store';
import * as UserActions from "./actions/user-actions"

class App extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			user: UserStore.getUser()
		}
	}

	componentWillMount() {
		UserStore.on("change", () => {
			this.setState({
				user: UserStore.getUser()
			});
		});
	}

	render() {
		var logout = "";
		if(this.state.user.loggedIn) {
			logout = <span className="logout" onClick={UserActions.logout}>Logout</span>
		}

		return (
			<Router>
				<div>
					<nav>
						<span className="page-name">Task App</span>
						{logout}
					</nav>
					{/* 
					<ul>
						<li><Link to="/">Home</Link></li>
						<li><Link to="/projects">Projects</Link></li>
						<li><Link to="/tasks">Tasks</Link></li>
						{ logout }
					</ul>
					*/}
					<Route exact path="/" component={Home}/>
					<Route path="/projects" component={ProjectList}/>
					<Route path="/project/:title" component={TaskList}/>
					
				</div>
			</Router>
		)
	}
}

const AppComp = withRouter(App);

window.addEventListener("DOMContentLoaded", function() {
	ReactDOM.render(<AppComp.WrappedComponent />, document.getElementById('root'));
});