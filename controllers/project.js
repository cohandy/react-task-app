var path = require('path'),
	async = require('async'),
	ProjectModel = require('../models/project'),
	TaskModel = require('../models/task');

module.exports = {
	fetch: function(req, res, next) {
		ProjectModel.find({userId: req.session.user._id}).limit(20).exec(function(err, projects) {
			if(err) return next(err);
			res.json({'success': 'true', projects: projects});
		});
	},
	create: function(req, res, next) {
		var newProject = new ProjectModel({
			title: req.body.title,
			userId: req.session.user._id
		});
		newProject.save(function(err, project) {
			if(err) {
				err.success = 'false';
				res.json(err);
			} else {
				res.json({'success': 'true', project: project});
			}
		});
	},
	delete: function(req, res, next) {
		var owner = false;
		async.series([
			function(callback) {
				//check if project is owned by the user in session
				ProjectModel.findOne({_id: req.body.projectId}).exec(function(err, project) {
					if(err) return callback(err);
					if(project && String(project.userId) === req.session.user._id) {
						owner = true;
					}
					callback();
				});
			},
			function(callback) {
				if(owner) {
					TaskModel.remove({projectId: req.body.projectId}).exec(function(err) {
						if(err) return callback(err);
						callback();
					});
				}
			}
		], function(err) {
			if(err) return next(err);
			if(owner) {
				ProjectModel.findOneAndRemove({_id: req.body.projectId}).exec(function(err) {
					if(err) return next(err);
					res.json({'success': 'true', '_id': req.body.projectId});
				});
			} else {
				res.json({'success': 'false', error: 'You must own this project to delete it!'})
			}
		});
	}
}