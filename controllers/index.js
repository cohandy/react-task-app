module.exports = {
	home: require('./home'),
	project: require('./project'),
	task: require('./task')
}