var path = require('path'),
	async = require('async'),
	ProjectModel = require('../models/project'),
	TaskModel = require('../models/task');

var TaskController = {
	fetch: function(req, res, next) {
		var owner = false;
		async.series([
			function(callback) {
				//check if project is owned by the user in session
				ProjectModel.findOne({_id: req.body.projectId}).exec(function(err, project) {
					if(err) return callback(err);
					if(project && String(project.userId) === req.session.user._id) {
						owner = true;
					}
					callback();
				});
			}
		], function(err) {
			if(err) return next(err);
			//if user owns project, fetch tasks
			if(owner) {
				TaskModel.find({projectId: req.body.projectId}).exec(function(err, tasks) {
					if(err) {
						res.json({'success': 'false'});
					} else {
						res.json({'success': 'true', tasks: tasks});
					}
				});
			} else {
				res.json({'success': 'false', error: 'You must own this project to add a task!'})
			}
		});
	},
	create: function(req, res, next) {
		var newTask = new TaskModel({
			task: req.body.task,
			projectId: req.body.projectId
		});
		var owner = false;
		async.series([
			function(callback) {
				//check if project is owned by the user in session
				ProjectModel.findOne({_id: req.body.projectId}).exec(function(err, project) {
					if(err) return callback(err);
					if(project && String(project.userId) === req.session.user._id) {
						owner = true;
					}
					callback();
				});
			}
		], function(err) {
			if(err) return next(err);
			//if user owns project, save task
			if(owner) {
				newTask.save(function(err, task) {
					if(err) {
						res.json({'success': 'false', error: err.errors.task.message});
					} else {
						res.json({'success': 'true', task: task});
					}
				});
			} else {
				res.json({'success': 'false', error: 'You must own this project to add a task!'})
			}
		});
	},
	complete: function(req, res, next) {
		var owner = false;
		async.series([
			function(callback) {
				//check if project is owned by the user in session
				ProjectModel.findOne({_id: req.body.projectId}).exec(function(err, project) {
					if(err) return callback(err);
					if(project && String(project.userId) === req.session.user._id) {
						owner = true;
					}
					callback();
				});
			}
		], function(err) {
			if(err) return next(err);
			//if user owns project, save task
			if(owner) {
				TaskModel.findOne({_id: req.body.taskId}).exec(function(err, task) {
					if(err) return next(err);
					task.completed = true;
					task.save();
					res.json({'success': 'true', task: task});
				});
			} else {
				res.json({'success': 'false', error: 'You must own this task to complete it!'})
			}
		});
	},
	delete: function(req, res, next) {
		var owner = false;
		async.series([
			function(callback) {
				//check if project is owned by the user in session
				ProjectModel.findOne({_id: req.body.projectId}).exec(function(err, project) {
					if(err) return callback(err);
					if(project && String(project.userId) === req.session.user._id) {
						owner = true;
					}
					callback();
				});
			}
		], function(err) {
			if(err) return next(err);
			//if user owns project, delete task
			if(owner) {
				TaskModel.remove({_id: req.body.taskId}).exec(function(err, task) {
					if(err) return next(err);
					res.json({'success': 'true', _id: req.body.taskId});
				});
			} else {
				res.json({'success': 'false', error: 'You must own this task to complete it!'})
			}
		});	
	}
}

module.exports = TaskController;