var path = require('path'),
	UserModel = require('../models/user');

module.exports = {
	index: function(req, res, next) {
		res.sendFile(path.join(__dirname, '../views/index.html'));
	},
	login: function(req, res, next) {
		UserModel.find({username: req.body.username}).exec(function(err, user) {
			if(err) return next(err);
			if(user.length) {
				//add session
				req.session.user = user[0];
				req.session.save();
				res.json({'success': 'true', 'user': user});
			} else {
				var newUser = new UserModel({
					username: req.body.username
				});
				newUser.save(function(err, user) {
					if(err) {
						err.success = 'false';
						res.json(err);
					} else {
						res.json({'success': 'true', 'user': user});
					}
				});
			}
		});
	},
	logout: function(req, res, next) {
		req.session.user = null;
		res.json({'success': 'true'});
	}
}